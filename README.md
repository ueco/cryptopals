## Cryptopals challenge solutions from http://cryptopals.com/

Requirements - python3

Tests (`pytest`) should be run within each `tests` directory, otherwise certain directories won't be seen.
I'll try to fix this in future, right now I'm focused on tasks.
