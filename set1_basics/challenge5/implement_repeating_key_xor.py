#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
from itertools import cycle
import binascii
sys.path.append('..')
from utils import utils

def implement_repeating_key_xor(phrase, key):
    testString = bytes(phrase, 'utf8')
    xorOut = [a ^ b for (a, b) in zip(testString, cycle(bytes(key, 'utf8')))]
    out = binascii.hexlify(bytes(xorOut))
    logging.debug("Out: {}".format(out))
    return out.decode('utf-8')

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    openingStanza = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
    xorAgainst = "ICE"
    logging.debug("Phrase to encode: {} | XOR against: {}".format(openingStanza, xorAgainst))
    implement_repeating_key_xor(openingStanza, xorAgainst)

if __name__ == "__main__":
    main()
