#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from set1_basics.challenge3 import single_byte_xor_cipher

def test_single_byte_xor_cipher():
    inputStr = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    outputLetter = "X"
    outputScore = 286.54360104874246
    outputCode = "Cooking MC's like a pound of bacon"
    assert single_byte_xor_cipher.single_byte_xor_cipher(inputStr) == (outputLetter, outputScore, outputCode)
