#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from set1_basics.challenge5 import implement_repeating_key_xor

def test_implement_repeating_key_xor():
    openingStanza = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
    xorAgainst = "ICE"
    output = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
    assert implement_repeating_key_xor.implement_repeating_key_xor(openingStanza, xorAgainst) == output
