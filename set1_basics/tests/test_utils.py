#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from binascii import unhexlify
from set1_basics.utils import utils

def test_power():
    assert utils.power(0) == 0
    assert utils.power(1) == 1*1
    assert utils.power(2) == 2*2
    assert utils.power(5) == 5*5
    assert utils.power(325) == 325*325

def test_expected_number_letters():
    string = "eeeeeEEEEE"
    assert utils.expected_number_letters('e', len(string)) == utils.etaoin['e'] * len(string)
    string = "aaaaaaaaaaaaaaaaaaa"
    assert utils.expected_number_letters('a', len(string)) == utils.etaoin['a'] * len(string)
    string = "zzzzzzzzZZZZZZZZZZZZZZZZZZZzzzzzzzz"
    assert utils.expected_number_letters('z', len(string)) == utils.etaoin['z'] * len(string)

def test_actual_number_letters():
    string = "eeeeeeeeeeeeeEEEEEE"
    assert utils.actual_number_letters(string, 'e') == string.count('e')
    string = "zzzzzZZZZZZZZzEEEEEEEeGGG"
    assert utils.actual_number_letters(string, 'Z') == string.count('Z')
    string = "oifjdsaoifjaoieodiwmaocmaoixz"
    assert utils.actual_number_letters(string, 'v') == string.count('v')

def test_count_penalty():
    string = "###"
    score = 255*3
    assert utils.count_penalty(string, 0) == score
    string = "%^&"
    score = 255*3
    assert utils.count_penalty(string, 0) == score
    string = "Some strange #$(ked up sentence!!!"
    score = 255*6
    assert utils.count_penalty(string.lower(), 0) == score

def test_count_chi_square_score():
    string = "aoljhlzhyjpwolypzvulvmaollhysplzaruvduhukzptwslzajpwolyzpapzhafwlvmzbizapabapvujpwolypudopjolhjoslaalypuaolwshpualeapzzopmalkhjlyahpuubtilyvmwshjlzkvduaolhswohila"
    score = 1755.050188063853
    assert utils.count_chi_square_score(string) == score
    string = "Defend the east wall of the castle"
    score = 13.505312389807285
    assert utils.count_chi_square_score(string) == score
    string = "Cooking MC's like a pound of bacon"
    score = 286.54360104874246
    assert utils.count_chi_square_score(string) == score

def test_hex_to_bytes_string():
    encoded = "666f6f"
    decoded = b"foo"
    assert utils.hex_to_bytes(encoded) == decoded
    encoded = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    decoded = b"I'm killing your brain like a poisonous mushroom"
    assert utils.hex_to_bytes(encoded) == decoded
    encoded = "686974207468652062756c6c277320657965"
    decoded = b"hit the bull's eye"
    assert utils.hex_to_bytes(encoded) == decoded
    encoded = "746865206b696420646f6e277420706c6179"
    decoded = b"the kid don't play"
    assert utils.hex_to_bytes(encoded) == decoded

def test_bytes_string_xor_against_single_char():
    bytesString = b"foo"
    xorAgainst = ord('a')
    assert utils.bytes_xor_against_char(bytesString, xorAgainst) == '\x07\x0e\x0e'
    bytesString = b"\x1b77316?x\x15\x1b\x7f+x413=x9x(7-6<x7>x:9;76"
    xorAgainst = ord('X')
    assert utils.bytes_xor_against_char(bytesString, xorAgainst) == "Cooking MC's like a pound of bacon"

