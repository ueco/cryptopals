#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from set1_basics.challenge1 import hex_to_base64

def test_hex_to_base64():
    string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    output = b"SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
    assert hex_to_base64.hex_to_base64(string) == output
