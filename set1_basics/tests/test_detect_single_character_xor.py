#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from set1_basics.challenge4 import detect_single_character_xor

def test_detect_single_character_xor_small():
    path = "../challenge4/test.txt"
    hexEncoded = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    outputKey = 'X'
    outputCode = "Cooking MC's like a pound of bacon"
    outputScore = 286.54360104874246
    assert detect_single_character_xor.detect_single_character_xor(path) == (hexEncoded, outputKey, outputScore, outputCode)

def test_detect_single_character_xor_big():
    path = "../challenge4/4.txt"
    hexEncoded = "7b5a4215415d544115415d5015455447414c155c46155f4058455c5b523f"
    outputKey = '5'
    outputCode = "Now that the party is jumping\n"
    outputScore = 299.8528142982048
    assert detect_single_character_xor.detect_single_character_xor(path) == (hexEncoded, outputKey, outputScore, outputCode)
