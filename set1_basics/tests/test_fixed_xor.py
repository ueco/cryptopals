#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from set1_basics.challenge2 import fixed_xor

def test_fixed_xor():
    inputStr1 = "1c0111001f010100061a024b53535009181c"
    inputStr2 = "686974207468652062756c6c277320657965"
    output = b"746865206b696420646f6e277420706c6179"
    assert fixed_xor.fixed_xor(inputStr1, inputStr2) == output
