#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from binascii import hexlify
import logging
import sys
sys.path.append('..')
from utils import utils

def fixed_xor(firstHex, secondHex):
    firstRaw = utils.hex_to_bytes(firstHex)
    logging.debug(firstRaw)
    secondRaw = utils.hex_to_bytes(secondHex)
    logging.debug(secondRaw)
    xorOut = [a ^ b for (a, b) in zip(firstRaw, secondRaw)]
    xorRaw = bytes(xorOut)
    xorHex = hexlify(xorRaw)
    logging.debug(xorRaw)
    logging.debug(xorHex)
    return xorHex

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    fixed_xor("1c0111001f010100061a024b53535009181c", "686974207468652062756c6c277320657965")

if __name__ == "__main__":
    main()
