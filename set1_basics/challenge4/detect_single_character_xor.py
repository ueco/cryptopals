#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
sys.path.append('..')
from challenge3 import single_byte_xor_cipher
from utils import utils

def detect_single_character_xor(path):
    hexEncoded = ""
    letter = ""
    score = sys.maxsize
    code = ""
    with open(path, 'r', encoding="latin-1") as f:
        for line in f.readlines():
            strippedLine = line.strip()
            l, s, c = single_byte_xor_cipher.single_byte_xor_cipher(strippedLine)
            if s < score:
                hexEncoded = strippedLine
                letter = l
                score = s
                code = c
    logging.debug("Probably winner:")
    logging.debug("hexEncoded: {}".format(strippedLine))
    logging.debug("Key: {} | Score: {} | Score: {}\n".format(letter, score, code))
    return hexEncoded, letter, score, code

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    detect_single_character_xor("4.txt")

if __name__ == "__main__":
    main()

