#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from base64 import b64encode
import logging
import sys
sys.path.append('..')
from utils import utils

def hex_to_base64(hexval):
    rawVal = utils.hex_to_bytes(hexval)
    logging.debug(rawVal)
    base64out = b64encode(rawVal)
    logging.debug(base64out)
    return base64out

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    hex_to_base64('49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d')

if __name__ == "__main__":
    main()
