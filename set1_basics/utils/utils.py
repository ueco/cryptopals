#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from binascii import unhexlify

etaoin = {'e': 0.127, 't': 0.090, 'a': 0.081, 'o': 0.075, 'i': 0.069, 'n': 0.067, ' ': 0.065,
        's': 0.063, 'h': 0.060, 'r': 0.059, 'd': 0.042, 'l': 0.040, 'c': 0.027,
        'u': 0.027, 'm': 0.024, 'w': 0.023, 'f': 0.022, 'g': 0.020, 'y': 0.019,
        'p': 0.019, 'b': 0.014, 'v': 0.009, 'k': 0.007, 'j': 0.001, 'x': 0.001,
        'q': 0.00095, 'z': 0.00074}

def power(i):
    return i*i

def expected_number_letters(etaoinLetter, stringLength):
    return etaoin[etaoinLetter] * stringLength

def actual_number_letters(s, c):
    return s.count(c)

def count_penalty(s, score):
    for c in s:
        if c not in etaoin:
            score += 255
    return score

def count_chi_square_score(string):
    chiSquare = 0
    sLen = len(string)
    smallString = string.lower()
    for char in etaoin:
        if char in smallString:
            expected = expected_number_letters(char, sLen)
            chiSquare += (power(actual_number_letters(smallString, char) - expected))/expected
    chiSquare = count_penalty(smallString, chiSquare)
    return chiSquare

def hex_to_bytes(hexVal):
    return unhexlify(hexVal)

def bytes_xor_against_char(bytesString, key):
    xorOut = (chr(a ^ key) for a in bytesString)
    return ''.join(xorOut)
