#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
sys.path.append('..')
from utils import utils

def single_byte_xor_cipher(hexEncoded):
    hexDecoded = utils.hex_to_bytes(hexEncoded)

    codes = {}
    scores = {}
    for i in range(ord('0'), ord('z')+1):
        codes[i] = utils.bytes_xor_against_char(hexDecoded, i)
        scores[i] = utils.count_chi_square_score(codes[i])

    best = min(scores, key=scores.get)
    logging.debug("Probably winner:")
    logging.debug("Key: {} | Score: {} | Result: {}\n".format(chr(best), scores[best], codes[best]))

    return chr(best), scores[best], codes[best]

def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    hexEncoded = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    logging.debug("hexEncoded: {}".format(hexEncoded))
    single_byte_xor_cipher(hexEncoded)

if __name__ == "__main__":
    main()
